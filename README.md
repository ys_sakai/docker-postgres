# docker-postgres
PostgreSQLのdocker構成

## Description
Dockerを使用したPostgreSQLの開発環境。  

## Requirement
- [docker-reverse-proxy](https://gitlab.com/ys_sakai/docker-reverse-proxy)

## Usage
```sh
# コンテナの起動
docker-compose up -d

# コンテナの停止
docker-compose stop

# コンテナの削除
docker-compose rm

# ボリュームの確認
docker volume ls

# ボリュームの削除
docker volume rm postgres_postgres-data 
```

## 参考
- [Docker for Windows で postgres コンテナの Volume マウントを安全にする](https://qiita.com/megmogmog1965/items/e7cd4500006c3b6b1894)
- [Docker PostgreSQL公式イメージを使用してDBに初期データを流し込む](https://qiita.com/furu8ma/items/c7e33ae34ef0216843b8)
